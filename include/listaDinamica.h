#ifndef _LISTA_DINAMICA_H_
#define _LISTA_DINAMICA_H_
#define NAME_SIZE 30

/****** tipos de dados da lista ******/
struct aluno{
    int matricula;
    char nome[NAME_SIZE];
}typedef ALUNO;

typedef struct no_descritor LISTA; // "Lista" é um nó descritor com as informações da lista

/****** Funções da lista ******/

// Cria a lista
LISTA* criarLista();

// Verifica se a lista está vazia
int listaVazia(LISTA* lista);

// Retorna tamanho da lista
int tamanhoLista(LISTA* lista);

// Insere um aluno na lista
int inserirAluno(LISTA* lista, ALUNO aluno);

// Remover aluno
int removerAluno(LISTA* lista, int matricula);

// Obter um aluno passando tipo "matricula ou posicao" e o número
int obterAluno(LISTA* lista, short tipo, int numero, ALUNO* alunoDestino);

// Lista todos os alunos em uma tabela
void listarTodosAlunos(LISTA* lista);

#endif
