#include <stdlib.h>
#include <stdio.h>
#include "listaDinamica.h"

/*************** tipos de dados locais ***********/

struct elemento_da_lista{ // Define como é um nó(elemento) desta lista
    struct aluno aluno; // Guarda um aluno
    struct elemento_da_lista *proximo; // Ponteiro para o próximo elemento da lista
}typedef ELEM;

struct no_descritor{ // Nó descritor com as informações da lista
    ELEM* inicio; // Aponta para o elemento que está início da lista
    ELEM* final; // Aposta para o elemento que está no final da lista
    int qtd;
};

/*************** funções da lista dinâmica encadeada com nó descritor **************/

// Cria a lista
LISTA* criarLista(){
    LISTA* lista = (LISTA*) malloc(sizeof(LISTA));
    if(lista != NULL){ // Verifica se a lista existe, não é nula, foi alocada na memória
        lista->inicio = NULL;
        lista->final = NULL;
        lista->qtd = 0;
    }
    return lista;
}

// Verifica se a lista está vazia
int listaVazia(LISTA* lista){
    if((lista == NULL) || (lista->qtd == 0)){
        // Uma lista que não existe não pode estar cheia, por isso é retornado 1
        return 1; // Está vazia
    }else{
        return 0; // Não está vazia
    }
}

// Retorna tamanho da lista
int tamanhoLista(LISTA* lista){
    if(lista == NULL) return 0; // Verifica se a lista é nula (não existe)
    
    return lista->qtd;
}

// Insere um aluno na lista
int inserirAluno(LISTA* lista, ALUNO aluno){
    if(lista == NULL){
        return 0; // Erro, lista não existe, não está alocada
    }else if(aluno.matricula > 9999){
        return -1; // Número de matrícula maior que 9999 não é permitido
    }

    if(aluno.matricula <= 0){ // Se a matrícula for inválida, gera nova matrícula automaticamente
        if(listaVazia(lista)){
            aluno.matricula = 1;
        }else{
            aluno.matricula = lista->final->aluno.matricula + 1; // Obtém matrícula no último aluno e incrementa 1
        }
    }

    ELEM* noAluno = (ELEM*) malloc(sizeof(ELEM));
    if(noAluno == NULL) return 0; // Se nó está nulo, ocorreu erro ao alocar memória
    noAluno->aluno = aluno;
    noAluno->proximo = NULL;

    if(listaVazia(lista)){// Se a lista estava vazio, inserção do 1º elemento
        lista->inicio = noAluno;
        lista->final = noAluno;  // o 1º elemento é inicio e final da lista
    }
    else{ // Procura onde inserir
        ELEM *noAnterior, *noAuxiliar = lista->inicio;
        while( (noAuxiliar != NULL) && (noAuxiliar->aluno.matricula < aluno.matricula) ){ // Percorre lista até que noAuxiliar->matrícula >= aluno.matrícula
            noAnterior = noAuxiliar;
            noAuxiliar = noAuxiliar->proximo; // "Incrementa", aponta p/ próximo elemento da lista
        }
        // Verifica resultado da busca
        if(noAuxiliar == lista->inicio){ // Estamos no início da lista
            lista->inicio = noAluno; // noAluno vira 1º da lista
            noAluno->proximo = noAuxiliar;  // noAluno → noAuxiliar → noSeguinte → ...
        }
        else{ // Estamos no meio ou final da lista
            if(noAuxiliar == NULL){ // Estamos no final da lista ?
                lista->final = noAluno; // Guarda o novo final da lista
            }
            // Insere aluno
            noAnterior->proximo = noAluno; // ... noAnterior → noAluno
            noAluno->proximo = noAuxiliar; // ... noAnterior → noAluno → noAuxiliar ...
        }
    }
    lista->qtd++; // Incrementa quantidade de alunos

    return 1;
}

// Remover aluno
int removerAluno(LISTA* lista, int matricula){
    if(listaVazia(lista)) return 0; // Erro, não é possível remover de lista vazia

    // Procura elemento
    ELEM *noAnterior, *noAuxiliar = lista->inicio;
    while( (noAuxiliar->proximo != NULL) && (noAuxiliar->aluno.matricula != matricula) ){ // Percorre lista até último nó não nulo ou encontrar matrícula
        noAnterior = noAuxiliar;
        noAuxiliar = noAuxiliar->proximo;
    }
    // Verifica resultado da busca
    if(noAuxiliar->aluno.matricula != matricula) return -1; // Não encontrou

    // Encontrou
    if(tamanhoLista(lista) == 1){// Remoção do único elemento
        lista->inicio = NULL;
        lista->final = NULL;
    }else if(noAuxiliar == lista->inicio){ // Ainda estamos no início da lista?
        lista->inicio = noAuxiliar->proximo; // Elemento seguinte vira o início da lista
    }
    else if(noAuxiliar == lista->final){ // Estamos no final da lista: ... noAnterior → noAuxiliar → NULL
        noAnterior->proximo = noAuxiliar->proximo; // ... noAnterior → NULL
        lista->final = noAnterior; // Final da lista vira noAnterior
    }
    else{ // Estamo no meio da lista : ... noAnterior → noAuxiliar → noSeguinte ...
        noAnterior->proximo = noAuxiliar->proximo; // ... noAnterior → noSeguinte ...
    }
    free(noAuxiliar); // Libera elemento que antes era o último da lista
    lista->qtd--; // Decrementa quantidade de alunos

    return 1;
}

// Obter um aluno passando tipo "matricula ou posicao" e o número
int obterAluno(LISTA* lista, short tipo, int numero, ALUNO* alunoDestino){
    if(listaVazia(lista)) return 0; // Erro, lista vazia
    if(numero <= 0) return -1; // Erro, número inválido

    ELEM *noAuxiliar = lista->inicio;
    if(tipo == 1){
        // Procura onde o aluno está pela matrícula
        while ((noAuxiliar != NULL) && (noAuxiliar->aluno.matricula != numero)){
            noAuxiliar = noAuxiliar->proximo; // Aponta para o próximo no da lista
        }
        // Verifica resultado da busca
        if(noAuxiliar != NULL){ // Encontrou
            *alunoDestino = noAuxiliar->aluno;
            return 1;
        }
    }else if(tipo == 2){
        // Procura o aluno pela posição
        unsigned int posicao = 1;
        while((noAuxiliar != NULL) && (posicao != numero)){
            noAuxiliar = noAuxiliar->proximo; // Aponta para o próximo no da lista
            posicao++; // Incrementa
        }
        // Verifica resultado da busca
        if(posicao == numero){ // Encontrou
            *alunoDestino = noAuxiliar->aluno;
            return 1;
        }
    }
    return -1; // Se chegou aqui, não encontrou
}

// Lista todos os alunos em uma tabela
void listarTodosAlunos(LISTA* lista){
    if(listaVazia(lista)){ // Se a lista vazia, sai da função
        return;
    }
    printf(
        "\n\033[7m"
        "+-------------------+-------------------------------+\n"
        "|     Matrícula     |             Aluno             |\n"
        "+-------------------+-------------------------------+\033[0m\n"
    );
    ELEM *noAuxiliar = lista->inicio;
    do{
        printf(
            "| %-17d | %-30s|\n"
            "+-------------------+-------------------------------+\n",
            noAuxiliar->aluno.matricula,
            noAuxiliar->aluno.nome
        );
        noAuxiliar = noAuxiliar->proximo;
    }while(noAuxiliar != NULL);
}
