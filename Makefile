# Compila o programa inteiro

# Variávies
CC = gcc
CFLAGS = -std=c99 -pedantic-errors -Wall

# Regra : dependências
all: listaDinamica.o presenca.o
	$(CC) $(CFLAGS) presenca.o include/listaDinamica.o -o presenca

presenca.o: presenca.c
	$(CC) $(CFLAGS) -c presenca.c -o presenca.o

listaDinamica.o: include/listaDinamica.h
	$(CC) $(CFLAGS) -c include/listaDinamica.c -o include/listaDinamica.o

clean:
	rm *.o
	rm include/*.o

run:
	./presenca

debug:
	make CFLAGS+=-g
