#include <stdio.h>
#include <locale.h>
#include "include/listaDinamica.h"
#define _GERAR_MATRICULA_ 0 // Define se a matrícula do aluno deve ser gerada automaticamente
int codigoRetornado, primeiraExecucao=1, codePageAnterior;
LISTA* lista;

#if defined(WIN32) || defined(_WIN32) || defined(__WIN32__) || defined(__NT__)
    #include <windows.h>
#endif

void limparTela(){
    printf("\033[H\033[2J\033[3J");
}
void clearStdinBuffer(){ // Limpa o buffer do stdIn
    while(getchar() != '\n');
}
int main(){
    unsigned int opcaoEscolhida;
    int numero=0;
    ALUNO aluno = {.matricula=-1, .nome="Aluno\0"};

    if(primeiraExecucao){
        #if defined(WIN32) || defined(_WIN32) || defined(__WIN32__) || defined(__NT__)
	        codePageAnterior = GetConsoleCP(); // Guarda CodePage anterior
            SetConsoleCP(65001);  // Ativa UTF-8 como CodePage de entrada
	        setlocale(LC_ALL, "pt_BR.UTF-8");
        #else
            setlocale(LC_ALL,"Portuguese");
        #endif
        lista = criarLista();
        primeiraExecucao = 0;
    }

    printf(
        "\033[5;32m======== Menu principal ========== \033[0m \n"
        "1 - Inserir alunos \n"
        "2 - Remover aluno \n"
        "3 - Consultar aluno \n"
        "4 - Exibir alunos \n"
        "0 - Sair \n"
        ">: "
    );
    opcaoEscolhida=13;
    scanf("%1u", &opcaoEscolhida);
    clearStdinBuffer();

    switch(opcaoEscolhida){
        case 1:
            limparTela();
            printf("\033[5;32m===== Inserir alunos ===== \033[0m \n");
            char opcao;
            do{
                aluno.matricula=-1; // Inicializa variável
                #if _GERAR_MATRICULA_ == 0
                    printf("Matrícula: ");
                    scanf("%d", &aluno.matricula);
                    clearStdinBuffer();
                    while(aluno.matricula <= 0){
                        printf("Matrícula inválida! Tente novamente.\nMatrícula: ");
                        scanf("%d", &aluno.matricula);
                        clearStdinBuffer();
                    }
                #else
                    // Define uma matrícula inválida. Assim, outra será gerada automaticamente
                    aluno.matricula = -1;
                #endif
                printf("Nome: ");
                scanf(" %[^\n]", aluno.nome);

                codigoRetornado = inserirAluno(lista, aluno);
                if(codigoRetornado == 0){
                    printf("Não foi possível inserir. \n");
                }
                if(codigoRetornado == -1){
                    printf(
                        "Limite de matrícula atingido. \n"
                        "Só é possível cadastrar matrícula <= 9999.\n"
                    );
                }
                printf("Continuar? \x1b[1;31m(s/n) \x1b[0m");
                scanf(" %c[^\n]", &opcao);
                clearStdinBuffer();
            }while(opcao != 'n');
            limparTela();
            main(); // Volta para main e mostra menu
            break;
        case 2:
            limparTela();
            printf(
                "\033[5;32m===== Remover aluno ===== \033[0m \n"
                "Matrícula: "
            );
            scanf("%d", &numero);
            clearStdinBuffer();

            codigoRetornado = removerAluno(lista, numero);

            limparTela();
            if(codigoRetornado == 0){
                printf("Erro ao remover aluno\n");
            }else if(codigoRetornado == -1){
                printf("Aluno não encontrado. \n");
            }else{
                printf("Aluno removido com sucesso. \n");
            }
            main(); // Volta para main e mostra menu
            break;
        case 3:
            limparTela();
            printf(
                "\033[5;32m===== Consultar aluno ===== \033[0m \n"
                "1 - Por matrícula \n"
                "2 - Por posição \n"
                ">: "
            );
            scanf("%1u", &opcaoEscolhida);
            clearStdinBuffer();
            // Verifica opção escolhida
            if(opcaoEscolhida == 1){
                printf("Matrícula: ");
            }else if(opcaoEscolhida == 2){
                printf("Posição: ");
            }else{
                limparTela();
                printf("Opção inválida \n");
                main();
                break;
            }
            scanf("%d", &numero);
            clearStdinBuffer();
            codigoRetornado = obterAluno(lista, opcaoEscolhida, numero, &aluno);

            limparTela();
            // Verifica retorna da função obterAluno()
            if(codigoRetornado == 0){
                printf("A lista está vazia. \n");
            }else if(codigoRetornado == -1){
                printf("Aluno não encontrado. \n");
            }else{
                printf(
                    "\033[5;32m==== Informações do aluno ==== \033[0m \n"
                    "Matrícula: %d \n"
                    "Nome: %s \n \n"
                    "Pressione enter para continuar... \n",
                    aluno.matricula,
                    aluno.nome
                );
                while(getchar() != '\n'); // Esperando "Enter" para continuar
                limparTela();
            }
            main(); // Volta para main e mostra menu
            break;
        case 4:
            limparTela();
            printf("\033[5;32m=================== Exibir alunos ===================\033[0m \n");

            if(listaVazia(lista)){
                limparTela();
                printf("A lista está vazia. \n");
            }else{
                listarTodosAlunos(lista);
                printf("Pressione enter para continuar... \n");
                while(getchar() != '\n'); // Esperando "Enter" para continuar
                limparTela();
            }
            main(); // Volta para main e mostra menu
            break;
        case 0:
            //limparTela();
            printf("Saindo do programa... \n");
            #if defined(WIN32) || defined(_WIN32) || defined(__WIN32__) || defined(__NT__)
                SetConsoleCP(codePageAnterior); // Retorna o codePage ao estado anterior a execução do programa
            #endif
            break; // Sai do switch finalizando o programa
        default:
            limparTela();
            printf("Opção inválida! Escolha novamente \n");
            main(); // Volta para main e mostra menu
    };
    return 0;
}
