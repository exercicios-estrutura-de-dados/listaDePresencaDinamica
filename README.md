# Lista dinâmica

Este é um exercício que foi proposto na disciplina de **Estrutura de Dados 1** do [**Instituto de Informática**](https://inf.ufg.br/) da [**Universidade Federal de Goiás**](https://www.ufg.br/).

Consiste num programa em C que guarda uma lista de alunos. Foi implementado usando uma lista dinâmica encadeada.

A lista de presença permite:
- Inserir alunos
- Remover aluno
- Consultar aluno (por matrícula ou posição na lista)
- Exibir todos os alunos

Cada aluno da lista contém:
- Matrícula
- Nome

O programa limita o número de matrícula até ≤ 9999.

Também é possível ativar ou desativar geração automática de matrícula mudando a definição `_GERAR_MATRICULA_` no arquivo [**presenca.c**](./presenca.c).
* ```#define _GERAR_MATRICULA_ 1``` // geração automática ativada
* ```#define _GERAR_MATRICULA_ 0``` // geração automática desativada

Este é o menu principal do programa
```c
======== Menu principal ==========
1 - Inserir alunos
2 - Remover aluno
3 - Consultar aluno
4 - Exibir alunos
0 - Sair
>: 
```

## Como compilar e executar?

Os requisitos são:
* [Git](https://git-scm.com/)
* [GNU Make](https://www.gnu.org/software/make/)
* Um Compilador para **C**
    * [GCC](https://gcc.gnu.org/) ou
    * [Clang](https://clang.llvm.org/) ou
    * [MinGW](https://www.mingw-w64.org/)

### Instalação das ferramentas
* No **Windows** utilizando [Chocolatey](https://chocolatey.org/install#individual).

Execute o comando no PowerShell
```powershell
> choco install git mingw make
```

* No **Ubuntu, Debian, Mint, PopOS**...</br>

Execute o comando no Terminal
```bash
$ sudo apt update && sudo apt install git build-essential
```

* No **MacOS** </br>

Instale [Command Line Tools](https://developer.apple.com/download/all/) ou [Xcode](https://developer.apple.com/download/all/)

### Compilando e executando
Basta clonar o repositório e utilizar o make para compilar
```bash
$ git clone https://gitlab.com/exercicios-estrutura-de-dados/listaDePresencaDinamica.git
$ cd listaDePresencaDinamica/
$ make
$ ./presenca
```

## License
This project is licensed under the terms of the [MIT](https://choosealicense.com/licenses/mit/) license.
